import sys
import os
import time
import logging
import traceback
import numpy as np
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMessageBox
import shlex
import subprocess
import requests
import glob
import webrtcvad
import logging
from deepspeech import Model
from timeit import default_timer as timer
import collections
import contextlib
import wave
import ffmpeg
import uuid
import json

import urllib
import urllib3



UPLOAD_DIRECTORY = '/tmp'
ALLOWED_EXTENSIONS = set(['wav', 'mp3', 'flac'])
BASE_SERVER_ADDRESS = "http://127.0.0.1:8000/modelsDS/"

def read_wave(path):
    """Reads a .wav file.

    Takes the path, and returns (PCM audio data, sample rate).
    """
    with contextlib.closing(wave.open(path, 'rb')) as wf:
        num_channels = wf.getnchannels()
        assert num_channels == 1
        sample_width = wf.getsampwidth()
        assert sample_width == 2
        sample_rate = wf.getframerate()
        assert sample_rate in (8000, 16000, 32000)
        frames = wf.getnframes()
        pcm_data = wf.readframes(frames)
        duration = frames / sample_rate
        return pcm_data, sample_rate, duration


def write_wave(path, audio, sample_rate):
    """Writes a .wav file.

    Takes path, PCM audio data, and sample rate.
    """
    with contextlib.closing(wave.open(path, 'wb')) as wf:
        wf.setnchannels(1)
        wf.setsampwidth(2)
        wf.setframerate(sample_rate)
        wf.writeframes(audio)


class Frame(object):
    """Represents a "frame" of audio data."""
    def __init__(self, bytes, timestamp, duration):
        self.bytes = bytes
        self.timestamp = timestamp
        self.duration = duration


def frame_generator(frame_duration_ms, audio, sample_rate):
    """Generates audio frames from PCM audio data.

    Takes the desired frame duration in milliseconds, the PCM data, and
    the sample rate.

    Yields Frames of the requested duration.
    """
    n = int(sample_rate * (frame_duration_ms / 1000.0) * 2)
    offset = 0
    timestamp = 0.0
    duration = (float(n) / sample_rate) / 2.0
    while offset + n < len(audio):
        yield Frame(audio[offset:offset + n], timestamp, duration)
        timestamp += duration
        offset += n


def vad_collector(sample_rate, frame_duration_ms,
                  padding_duration_ms, vad, frames):
    """Filters out non-voiced audio frames.

    Given a webrtcvad.Vad and a source of audio frames, yields only
    the voiced audio.

    Uses a padded, sliding window algorithm over the audio frames.
    When more than 90% of the frames in the window are voiced (as
    reported by the VAD), the collector triggers and begins yielding
    audio frames. Then the collector waits until 90% of the frames in
    the window are unvoiced to detrigger.

    The window is padded at the front and back to provide a small
    amount of silence or the beginnings/endings of speech around the
    voiced frames.

    Arguments:

    sample_rate - The audio sample rate, in Hz.
    frame_duration_ms - The frame duration in milliseconds.
    padding_duration_ms - The amount to pad the window, in milliseconds.
    vad - An instance of webrtcvad.Vad.
    frames - a source of audio frames (sequence or generator).

    Returns: A generator that yields PCM audio data.
    """
    num_padding_frames = int(padding_duration_ms / frame_duration_ms)
    # We use a deque for our sliding window/ring buffer.
    ring_buffer = collections.deque(maxlen=num_padding_frames)
    # We have two states: TRIGGERED and NOTTRIGGERED. We start in the
    # NOTTRIGGERED state.
    triggered = False

    voiced_frames = []
    for frame in frames:
        is_speech = vad.is_speech(frame.bytes, sample_rate)

        if not triggered:
            ring_buffer.append((frame, is_speech))
            num_voiced = len([f for f, speech in ring_buffer if speech])
            # If we're NOTTRIGGERED and more than 90% of the frames in
            # the ring buffer are voiced frames, then enter the
            # TRIGGERED state.
            if num_voiced > 0.9 * ring_buffer.maxlen:
                triggered = True
                # We want to yield all the audio we see from now until
                # we are NOTTRIGGERED, but we have to start with the
                # audio that's already in the ring buffer.
                for f, s in ring_buffer:
                    voiced_frames.append(f)
                ring_buffer.clear()
        else:
            # We're in the TRIGGERED state, so collect the audio data
            # and add it to the ring buffer.
            voiced_frames.append(frame)
            ring_buffer.append((frame, is_speech))
            num_unvoiced = len([f for f, speech in ring_buffer if not speech])
            # If more than 90% of the frames in the ring buffer are
            # unvoiced, then enter NOTTRIGGERED and yield whatever
            # audio we've collected.
            if num_unvoiced > 0.9 * ring_buffer.maxlen:
                triggered = False
                yield b''.join([f.bytes for f in voiced_frames])
                ring_buffer.clear()
                voiced_frames = []
    if triggered:
        pass
    # If we have any leftover voiced audio when we run out of input,
    # yield it.
    if voiced_frames:
        yield b''.join([f.bytes for f in voiced_frames])


'''
Load the pre-trained model into the memory
@param models: Output Grapgh Protocol Buffer file
@param scorer: Scorer file

@Retval
Returns a list [DeepSpeech Object, Model Load Time, Scorer Load Time]
'''
def load_model(models, scorer):
    model_load_start = timer()
    ds = Model(models)
    model_load_end = timer() - model_load_start
    logging.debug("Loaded model in %0.3fs." % (model_load_end))

    scorer_load_start = timer()
    ds.enableExternalScorer(scorer)
    scorer_load_end = timer() - scorer_load_start
    logging.debug('Loaded external scorer in %0.3fs.' % (scorer_load_end))

    return [ds, model_load_end, scorer_load_end]

'''
Run Inference on input audio file
@param ds: Deepspeech object
@param audio: Input audio for running inference on
@param fs: Sample rate of the input audio file

@Retval:
Returns a list [Inference, Inference Time, Audio Length]

'''
def stt(ds, audio, fs):
    inference_time = 0.0
    audio_length = len(audio) * (1 / fs)

    # Run Deepspeech
    logging.debug('Running inference...')
    inference_start = timer()
    output = ds.stt(audio)
    inference_end = timer() - inference_start
    inference_time += inference_end
    logging.debug('Inference took %0.3fs for %0.3fs audio file.' % (inference_end, audio_length))

    return [output, inference_time]

'''
Resolve directory path for the models and fetch each of them.
@param dirName: Path to the directory containing pre-trained models

@Retval:
Retunns a tuple containing each of the model files (pb, scorer)
'''
def resolve_models(dirName):
    pb = glob.glob(dirName + "/*.pbmm")[0]
    logging.debug("Found Model: %s" % pb)

    scorer = glob.glob(dirName + "/*.scorer")[0]
    logging.debug("Found scorer: %s" % scorer)

    return pb, scorer

'''
Generate VAD segments. Filters out non-voiced audio frames.
@param waveFile: Input wav file to run VAD on.0

@Retval:
Returns tuple of
    segments: a bytearray of multiple smaller audio frames
              (The longer audio split into mutiple smaller one's)
    sample_rate: Sample rate of the input audio file
    audio_length: Duraton of the input audio file

'''
def vad_segment_generator(wavFile, aggressiveness):
    logging.debug("Caught the wav file @: %s" % (wavFile))
    audio, sample_rate, audio_length = read_wave(wavFile)
    assert sample_rate == 16000, "Only 16000Hz input WAV files are supported for now!"
    vad = webrtcvad.Vad(int(aggressiveness))
    frames = frame_generator(30, audio, sample_rate)
    frames = list(frames)
    segments = vad_collector(sample_rate, 30, 300, vad, frames)

    return segments, sample_rate, audio_length





# Debug helpers
logging.basicConfig(stream=sys.stderr,
                    level=logging.DEBUG,
                    format='%(filename)s - %(funcName)s@%(lineno)d %(name)s:%(levelname)s  %(message)s')


class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.
    Supported signals are:

    finished:
        No data

    error
       'tuple' (ecxtype, value, traceback.format_exc())

    result
        'object' data returned from processing, anything

    progress
            'object' indicating the transcribed result
    '''

    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(object)
    #convert = pyqtSignal(object)


class Worker(QRunnable):
    '''
    Worker Thread

    Inherits from QRunnable to handle worker thread setup, signals and wrap-up

    @param callback:
    The funtion callback to run on this worker thread.
    Supplied args and kwargs will be passed through the runner.
    @type calllback: function
    @param args: Arguments to pass to the callback function
    @param kwargs: Keywords to pass to the callback function
    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store the conctructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        '''
        Initialise the runner function with the passed args, kwargs
        '''

        # Retrieve args/kwargs here; and fire up the processing using them
        try:
            transcript = self.fn(*self.args, **self.kwargs)
        except:
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            # Return the result of the processing
            self.signals.result.emit(transcript)
        finally:
            # Done
            self.signals.finished.emit()

class TextToTreeItem:

    def __init__(self):
        self.text_list = []
        self.titem_list = []

    def append(self, text_list, itemSet):
        for text in text_list:
            self.text_list.append(text)
            self.titem_list.append(itemSet)



class Json_view(QWidget):
    HEADER = ["Deepspeech Models Details", "Values"]

    def __init__(self,data):
        QWidget.__init__(self, flags=Qt.Widget)
        self.setWindowTitle("JSON_Model_View")
        self.setMaximumWidth(1500)
        self.setMaximumWidth(1500)
        self.initUI(data)

    def initUI(self,data):

        self.text_to_titem = TextToTreeItem()

        self.tree = QTreeWidget(self)
        self.tree.setColumnCount(1)
        self.tree.setColumnWidth(0, 300)
        self.tree.setColumnWidth(1, 300)
        self.tree.setHeaderLabels(self.HEADER)

        self.tree.header().setStyleSheet('QWidget { font: bold italic large "Times New Roman"; font-size: 12pt }')
        self.tree.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tree.setAlternatingRowColors(True)
        self.fnt = QFont()
        self.fnt.setPixelSize(18)
        self.tree.setFont(self.fnt)

        #self.tree.setStyleSheet("alternate-background-color: yellow;background-color: white;")
        self.tree.itemSelectionChanged.connect(self.download_click)


        self.root_item = QTreeWidgetItem(self.tree)
        self.recurse_data(data, self.root_item)
        self.tree.addTopLevelItem(self.root_item)
        self.root_item.setExpanded(True)


        self.root_item1 = QLabel("DS Versions")

        self.childItems = []
        for i in range(0, len(data)):
            self.childItems.append(QTreeWidgetItem())
            self.root_item.addChild(self.childItems[i])
        self.tree.setItemWidget(self.root_item, 0, self.root_item1)


        layout = QHBoxLayout()
        layout.addWidget(self.tree)


        main_layout = QBoxLayout(QBoxLayout.TopToBottom)
        main_layout.addLayout(layout)
        self.setLayout(main_layout)


    def recurse_data(self, data, tree):

        if isinstance(data, dict):
            for key, val in data.items():
                self.tree_add_row(key, val, tree)
        elif isinstance(data, list):
            for i, val in enumerate(data):
                key = str(i)
                self.tree_add_row(key, val, tree)
        else:
            print("Error")

    def tree_add_row(self, key, val, tree):

        text_list = []

        if isinstance(val, dict) or isinstance(val, list):
            text_list.append(key)
            row_item = QTreeWidgetItem([key])
            self.recurse_data(val, row_item)

        else:
            val = str(val)
            if key == 'file_url':
                val += "     (Click to download) "

            text_list.append(key)
            text_list.append(val)
            row_item = QTreeWidgetItem([key, str(val)])
        tree.addChild(row_item)
        self.text_to_titem.append(text_list, row_item)

    def download_click(self):
        getSelected = self.tree.selectedItems()
        if getSelected:
            baseNode = getSelected[0]
            key=baseNode.text(0)
            url = baseNode.text(1)
            if key == "file_url":
                self.openUrl(url.split()[0])

    def openUrl(self,url):
        url = QUrl(str(url))
        if not QDesktopServices.openUrl(url):
            QMessageBox.warning(self, 'Open Url', 'Error open url')



class App(QMainWindow):
    dirName = ""

    def __init__(self):
        super().__init__()
        self.title = 'Deepspeech Transcriber'
        self.left = 10
        self.top = 10
        self.width = 480
        self.height = 400
        self.initUI()


    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        layout = QGridLayout()
        layout.setSpacing(10)

        self.microphone = QRadioButton("Microphone")
        self.fileUpload = QRadioButton("File Upload")
        self.browseBox = QLineEdit(self, placeholderText="Wave File, Mono @ 16 kHz, 16bit Little-Endian")
        self.modelsBox = QLineEdit(self, placeholderText="Directory path for output_graph and scorer")
        self.textboxTranscript = QPlainTextEdit(self, placeholderText="Transcription")
        self.browseButton = QPushButton('Browse', self)
        self.browseButton.setToolTip('Select a wav file')
        self.modelsButton = QPushButton('Browse', self)
        self.modelsButton.setToolTip('Select deepspeech models folder')
        self.transcribeWav = QPushButton('Transcribe Wav', self)
        self.transcribeWav.setToolTip('Start Wav Transcription')
        self.openMicrophone = QPushButton('Start Speaking', self)
        self.openMicrophone.setToolTip('Open Microphone')
        self.listModel = QPushButton('GetModel', self)
        self.chooseList = QComboBox()


        self.cursor = QTextCursor()


        layout.addWidget(self.microphone, 0, 0, 1, 2)
        layout.addWidget(self.fileUpload, 0, 1, 1, 2)
        layout.addWidget(self.chooseList, 0, 2, 1, 2)
        layout.addWidget(self.listModel, 0, 4, 1, 1)
        layout.addWidget(self.browseBox, 1, 0, 1, 4)
        layout.addWidget(self.browseButton, 1, 4)
        layout.addWidget(self.modelsBox, 2, 0, 1, 4)
        layout.addWidget(self.modelsButton, 2, 4)
        layout.addWidget(self.transcribeWav, 3, 1, 1, 1)
        layout.addWidget(self.openMicrophone, 3, 3, 1, 1)
        layout.addWidget(self.textboxTranscript, 5, 0, -1, 0)
        self.chooseList.addItem("French")
        self.chooseList.addItem("English")
        self.chooseList.addItem("Spanish")
        self.chooseList.addItem("Italien")

        w = QWidget()
        w.setLayout(layout)

        self.setCentralWidget(w)

        # Microphone
        self.microphone.clicked.connect(self.mic_activate)

        # File Upload
        self.fileUpload.clicked.connect(self.wav_activate)

        # Connect Browse Button to Function on_click
        self.browseButton.clicked.connect(self.browse_on_click)

        # Connect the Models Button
        self.modelsButton.clicked.connect(self.models_on_click)

        # Connect Transcription button to threadpool
        self.transcribeWav.clicked.connect(self.transcriptionStart_on_click)

        #Connect List Model button to threadpool
        self.listModel.clicked.connect(self.list_model_deepspeech)
        #Connect ComboBox to threadpool
        self.chooseList.currentIndexChanged[str].connect(self.selectionchange)


        # Connect Microphone button to threadpool
        self.openMicrophone.clicked.connect(self.openMicrophone_on_click)
        self.openMicrophone.setCheckable(True)
        self.openMicrophone.toggle()

        self.browseButton.setEnabled(False)
        self.browseBox.setEnabled(False)
        self.modelsBox.setEnabled(False)
        self.modelsButton.setEnabled(False)
        self.transcribeWav.setEnabled(False)
        self.openMicrophone.setEnabled(False)

        self.show()

        # Setup Threadpool
        self.threadpool = QThreadPool()
        logging.debug("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())




    @pyqtSlot()
    def selectionchange(self):
        language_obj = str(self.chooseList.currentText())


    @pyqtSlot()
    def list_model_deepspeech(self):

        choice_language = str(self.chooseList.currentText())
        if choice_language == "French":
            api_url = BASE_SERVER_ADDRESS + "Fr"
        elif choice_language == "English":
            api_url = BASE_SERVER_ADDRESS + "English"
        elif choice_language == "Spanish":
            api_url = BASE_SERVER_ADDRESS + "Spanish"
        elif choice_language == "Italien":
            api_url = BASE_SERVER_ADDRESS +"Italien"

        rq = requests.get(api_url)
        res = rq.json()

        self.json_view = Json_view(res)
        self.json_view.show()



    @pyqtSlot()
    def mic_activate(self):
        logging.debug("Enable streaming widgets")
        self.en_mic = True
        self.browseButton.setEnabled(False)
        self.browseBox.setEnabled(False)
        self.modelsBox.setEnabled(True)
        self.modelsButton.setEnabled(True)
        self.transcribeWav.setEnabled(False)
        self.openMicrophone.setStyleSheet('QPushButton {background-color: #70cc7c; color: black;}')
        self.openMicrophone.setEnabled(True)

    @pyqtSlot()
    def wav_activate(self):
        logging.debug("Enable wav transcription widgets")
        self.en_mic = False
        self.openMicrophone.setStyleSheet('QPushButton {background-color: #f7f7f7; color: black;}')
        self.openMicrophone.setEnabled(False)
        self.browseButton.setEnabled(True)
        self.browseBox.setEnabled(True)
        self.modelsBox.setEnabled(True)
        self.modelsButton.setEnabled(True)

    @pyqtSlot()
    def browse_on_click(self):
        logging.debug('Browse button clicked')
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fileName, _ = QFileDialog.getOpenFileName(self, "Select wav file to be Transcribed", "","All Files (*.wav *.mp3 .*flac)")
        if self.fileName:
            self.browseBox.setText(self.fileName)
            self.transcribeWav.setEnabled(True)
            logging.debug(self.fileName)


    @pyqtSlot()
    def models_on_click(self):
        logging.debug('Models Browse Button clicked')
        self.dirName = QFileDialog.getExistingDirectory(self, "Select deepspeech models directory")
        if self.dirName:
            self.modelsBox.setText(self.dirName)
            logging.debug(self.dirName)

            # Threaded signal passing worker functions
            worker = Worker(self.modelWorker, self.dirName)
            worker.signals.result.connect(self.modelResult)
            worker.signals.finished.connect(self.modelFinish)
            worker.signals.progress.connect(self.modelProgress)

            # Execute
            self.threadpool.start(worker)
        else:
            logging.critical("*****************************************************")
            logging.critical("Model path not specified..")
            logging.critical("*****************************************************")
            return "Transcription Failed, models path not specified"

    def modelWorker(self, dirName, progress_callback):
        self.textboxTranscript.setPlainText("Loading Models...")
        self.openMicrophone.setStyleSheet('QPushButton {background-color: #f7f7f7; color: black;}')
        self.openMicrophone.setEnabled(False)
        self.show()
        time.sleep(1)
        return dirName

    def modelProgress(self, s):
        # FixMe: Write code to show progress here
        pass

    def modelResult(self, dirName):
        # Fetch and Resolve all the paths of model files
        output_graph, scorer = resolve_models(dirName)
        # Load output_graph, alphabet and scorer
        self.model = load_model(output_graph, scorer)

    def modelFinish(self):
        # self.timer.stop()
        self.textboxTranscript.setPlainText("Loaded Models, start transcribing")
        if self.en_mic is True:
            self.openMicrophone.setStyleSheet('QPushButton {background-color: #70cc7c; color: black;}')
            self.openMicrophone.setEnabled(True)
        self.show()

    @pyqtSlot()
    def transcriptionStart_on_click(self):
        logging.debug('Transcription Start button clicked')

        # Clear out older data
        self.textboxTranscript.setPlainText("")
        self.show()
        # Threaded signal passing worker functions
        worker = Worker(self.wavWorker, self.fileName)
        worker.signals.progress.connect(self.progress)
        worker.signals.result.connect(self.transcription)
        worker.signals.finished.connect(self.wavFinish)

        # Execute
        self.threadpool.start(worker)

    @pyqtSlot()
    def openMicrophone_on_click(self):
        logging.debug('Preparing to open microphone...')

        # Clear out older data
        self.textboxTranscript.setPlainText("")
        self.show()

        # Threaded signal passing worker functions
        # Prepare env for capturing from microphone and offload work to micWorker worker thread
        if (not self.openMicrophone.isChecked()):
            self.openMicrophone.setStyleSheet('QPushButton {background-color: #C60000; color: black;}')
            self.openMicrophone.setText("Stop")
            logging.debug("Start Recording pressed")
            logging.debug("Preparing for transcription...")

            sctx = self.model[0].createStream()
            subproc = subprocess.Popen(shlex.split('rec -q -V0 -e signed -L -c 1 -b 16 -r 16k -t raw - gain -2'),
                                       stdout=subprocess.PIPE,
                                       bufsize=0)
            self.textboxTranscript.insertPlainText('You can start speaking now\n\n')
            self.show()
            logging.debug('You can start speaking now')
            context = (sctx, subproc, self.model[0])

            # Pass the state to streaming worker
            worker = Worker(self.micWorker, context)
            worker.signals.progress.connect(self.progress)
            worker.signals.result.connect(self.transcription)
            worker.signals.finished.connect(self.micFinish)

            # Execute
            self.threadpool.start(worker)
        else:
            logging.debug("Stop Recording")

    '''
    Capture the audio stream from the microphone.
    The context is prepared by the openMicrophone_on_click()
    @param Context: Is a tuple containing three objects
                    1. Speech samples, sctx
                    2. subprocess handle
                    3. Deepspeech model object
    '''
    def micWorker(self, context, progress_callback):
        # Deepspeech Streaming will be run from this method
        logging.debug("Recording from your microphone")
        while (not self.openMicrophone.isChecked()):
            data = context[1].stdout.read(512)
            context[0].feedAudioContent(np.frombuffer(data, np.int16))
        else:
            transcript = context[0].finishStream()
            context[1].terminate()
            context[1].wait()
            self.show()
            progress_callback.emit(transcript)
            return "\n*********************\nTranscription Done..."

    def micFinish(self):
        self.openMicrophone.setText("Start Speaking")
        self.openMicrophone.setStyleSheet('QPushButton {background-color: #70cc7c; color: black;}')

    def transcription(self, out):
        logging.debug("%s" % out)
        self.textboxTranscript.insertPlainText(out)
        self.show()

    def wavFinish(self):
        logging.debug("File processed")

    def progress(self, chunk):
        logging.debug("Progress: %s" % chunk)
        self.textboxTranscript.insertPlainText(chunk)
        self.show()
    '''
    def allowed_file(filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    def convert_audiofile(self,file):
        filename = str(uuid.uuid4()) + ".wav"
        fileLocation = os.path.join(UPLOAD_DIRECTORY, filename)
        # import pdb;pdb.set_trace()
        stream = ffmpeg.input(file)
        stream = ffmpeg.output(stream, fileLocation, acodec='pcm_s16le', ac=1, ar='16k')
        ffmpeg.run(stream)
        return filename
        '''
    def wavWorker(self, waveFile, progress_callback):
        # Deepspeech will be run from this method
        logging.debug("Preparing for transcription...")
        inference_time = 0.0
        #convert 'waveFile' to pcm 16k 16b mono with ffmpeg
        #converted_audio = self.convert_audiofile(waveFile) # to verify
        #import pdb;pdb.set_trace()
        # Run VAD on the input file
        segments, sample_rate, audio_length = vad_segment_generator(waveFile, 1) #return output conversion function despite of wavefile here
        f = open(waveFile.rstrip(".wav") + ".txt", 'w')
        logging.debug("Saving Transcript @: %s" % waveFile.rstrip(".wav") + ".txt")

        for i, segment in enumerate(segments):
            # Run deepspeech on the chunk that just completed VAD
            logging.debug("Processing chunk %002d" % (i,))
            audio = np.frombuffer(segment, dtype=np.int16)
            output = stt(self.model[0], audio, sample_rate)
            inference_time += output[1]

            f.write(output[0] + " ")
            progress_callback.emit(output[0] + " ")

        # Summary of the files processed
        f.close()

        # Format pretty, extract filename from the full file path
        filename, ext = os.path.split(os.path.basename(waveFile))
        title_names = ['Filename', 'Duration(s)', 'Inference Time(s)', 'Model Load Time(s)', 'Scorer Load Time(s)']
        logging.debug("************************************************************************************************************")
        logging.debug("%-30s %-20s %-20s %-20s %s" % (title_names[0], title_names[1], title_names[2], title_names[3], title_names[4]))
        logging.debug("%-30s %-20.3f %-20.3f %-20.3f %-0.3f" % (filename + ext, audio_length, inference_time, self.model[1], self.model[2]))
        logging.debug("************************************************************************************************************")
        print("\n%-30s %-20s %-20s %-20s %s" % (title_names[0], title_names[1], title_names[2], title_names[3], title_names[4]))
        print("%-30s %-20.3f %-20.3f %-20.3f %-0.3f" % (filename + ext, audio_length, inference_time, self.model[1], self.model[2]))

        return "\n*********************\nTranscription Done..."


def main(args):
    app = QApplication(sys.argv)
    w = App()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main(sys.argv[1:])
